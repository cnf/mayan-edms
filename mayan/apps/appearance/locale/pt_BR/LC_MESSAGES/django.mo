��    $      <  5   \      0     1     7  	   ?  P   I     �     �     �     �     �     �  !   �     
       
         +     D  &   J     q  
   t          �  '   �  %   �     �     �  1   �          5     <  &   I     p     x  5   |  O   �       �       �     �     �  Z   �       	   %     /     C     I     c  "   v     �     �     �     �     �  %   �     	  
   	     	     	  $   /	  %   T	     z	     �	  =   �	     �	     �	     �	  "   
     $
     ,
  <   0
  M   m
     �
                                                $          "                                        
         !   	                          #                                            About Actions Anonymous Be sure to change the password to increase security and to disable this message. Cancel Confirm Confirm delete Create Details for: %(object)s Edit: %(object)s Email: <strong>%(email)s</strong> First time login Home Identifier Insufficient permissions Login Login using the following credentials: No No results None Page not found Password: <strong>%(password)s</strong> Released under the Apache 2.0 License Save Search Sorry, but the requested page could not be found. Space separated terms Submit User details Username: <strong>%(account)s</strong> Version Yes You don't have enough permissions for this operation. You have just finished installing <strong>Mayan EDMS</strong>, congratulations! required Project-Id-Version: Mayan EDMS
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-04-27 14:09-0400
PO-Revision-Date: 2016-03-21 21:06+0000
Last-Translator: Roberto Rosario
Language-Team: Portuguese (Brazil) (http://www.transifex.com/rosarior/mayan-edms/language/pt_BR/)
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 Sobre Ações Anônimo Certifique-se de alterar a senha para aumentar a segurança e para desativar esta mensagem Cancelar Confirmar Confirmar Exclusão Criar Detalhes para: %(object)s Editar: %(object)s E-mail: <strong>%(email)s</strong> Primeiro login inicio Identificador Permissão insuficiente Login Entre usando as seguintes credenciais não resultados Nenhum Pagina não encontrada Senha: <strong>%(password)s</strong> Lançado sob a licença Apache 2.0 14 Salvar Pesquisa Desculpe, mas a página solicitada não pôde ser encontrado. Termos de espaço separado Submeter Detalhes do usuário Nome: <strong>%(account)s</strong> Versão Sim Você não tem permissões suficientes para essa operação. Você acaba de terminar de instalar <strong> Maia EDMS </ strong>, parabéns! exigido 