��    $      <  5   \      0  :   1     l     r     v     �  #   �  	   �     �     �  (   �          %     9     >     G     M     V     ^     m     u     �     �     �     �  	   �     �     �     �     �                         #     7  �  L  ]   �     I  	   O     Y     q  1   �  
   �     �  $   �  8   �     %     5     J     R     b     g     n     w     �  #   �     �     �     �     �  	   �     �     �     	     /	     L	     U	     a	     j	      	      �	                                                                         #              	             $                                                 !              "       
          A storage backend that all workers can use to share files. About Add Available attributes:  Current user details Current user locale profile details Date time Days Edit current user details Edit current user locale profile details Edit details Edit locale profile File Filename Hours Language License Locale profile Minutes Must select at least one item. No action selected. None Object Remove Selection Setup Setup items Shared uploaded file Shared uploaded files Timezone Tools User User details User locale profile User locale profiles Project-Id-Version: Mayan EDMS
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-04-27 14:10-0400
PO-Revision-Date: 2016-03-21 21:08+0000
Last-Translator: Roberto Rosario
Language-Team: Portuguese (Brazil) (http://www.transifex.com/rosarior/mayan-edms/language/pt_BR/)
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 Um backend de armazenamento que todos os trabalhadores podem usar para compartilhar arquivos. Sobre Adicionar Atributos disponíveis: Detalhes do usuário Usuário Atual - detalhes do perfil de localidade hora, data Dia Editar detalhes do usuário corrente Editar Usuário Atual - detalhes do perfil de localidade Editar Detalhes Editar perfil Idioma Arquivo Nome do arquivo Hora Lingua Licença Perfil Idioma Minutos Deve selecionar pelo menos um item. Nenhuma ação selecionada. Nenhum Objeto Remover Seleção Configuração Itens da Configuração Arquivo enviado Partilhado Arquivos enviados Partilhado Timezone Ferramentas Usuário Detalhes do usuário Perfil de localidade do usuário Perfis de localidade do usuário 