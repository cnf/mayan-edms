# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# Rogerio Falcone <rogerio@falconeit.com.br>, 2015
msgid ""
msgstr ""
"Project-Id-Version: Mayan EDMS\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-04-27 14:11-0400\n"
"PO-Revision-Date: 2016-03-21 21:09+0000\n"
"Last-Translator: Roberto Rosario\n"
"Language-Team: Portuguese (Brazil) (http://www.transifex.com/rosarior/mayan-"
"edms/language/pt_BR/)\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#: apps.py:31
msgid "Document states"
msgstr ""

#: apps.py:47
msgid "Initial state"
msgstr "Estado Inicial"

#: apps.py:48 apps.py:58 apps.py:68 apps.py:74
msgid "None"
msgstr "Nenhum"

#: apps.py:52
msgid "Current state"
msgstr "Estado corrente"

#: apps.py:56 apps.py:83 models.py:189
msgid "User"
msgstr "Usuário"

#: apps.py:62
msgid "Last transition"
msgstr "Última transação"

#: apps.py:66 apps.py:79
msgid "Date and time"
msgstr "data e hora"

#: apps.py:72 apps.py:99 models.py:79
msgid "Completion"
msgstr ""

#: apps.py:86 forms.py:39 links.py:79 models.py:187
msgid "Transition"
msgstr "Transações"

#: apps.py:90 forms.py:41 models.py:190
msgid "Comment"
msgstr "Comentário"

#: apps.py:95
msgid "Is initial state?"
msgstr "é  estado inicial?"

#: apps.py:103 models.py:105
msgid "Origin state"
msgstr "Estado Original"

#: apps.py:107 models.py:109
msgid "Destination state"
msgstr "Estado  de destino"

#: links.py:15 links.py:38 models.py:59 views.py:185
msgid "Workflows"
msgstr "Workflows"

#: links.py:20
msgid "Create workflow"
msgstr ""

#: links.py:25 links.py:46 links.py:63
msgid "Delete"
msgstr "Excluir"

#: links.py:29 models.py:25
msgid "Document types"
msgstr "Tipos de Documentos"

#: links.py:33 links.py:50 links.py:67
msgid "Edit"
msgstr "Editar"

#: links.py:41
msgid "Create state"
msgstr "Criar estado"

#: links.py:54
msgid "States"
msgstr "estado"

#: links.py:58
msgid "Create transition"
msgstr "Criar Transições"

#: links.py:71
msgid "Transitions"
msgstr "Transações"

#: links.py:75
msgid "Detail"
msgstr "Detalhes"

#: models.py:21 models.py:67 models.py:101
msgid "Label"
msgstr "Label"

#: models.py:58 models.py:65 models.py:99 models.py:126
msgid "Workflow"
msgstr "Workflow"

#: models.py:71
msgid ""
"Select if this will be the state with which you want the workflow to start "
"in. Only one state can be the initial state."
msgstr ""
"Selecione se este será o estado com o qual você deseja que o fluxo de "
"trabalho para começar em. Apenas um Estado pode ser o estado inicial."

#: models.py:73
msgid "Initial"
msgstr "Inicial"

#: models.py:77
msgid ""
"Enter the percent of completion that this state represents in relation to "
"the workflow. Use numbers without the percent sign."
msgstr ""

#: models.py:92
msgid "Workflow state"
msgstr "Estado do Workflow"

#: models.py:93
msgid "Workflow states"
msgstr "Estados do Workflow"

#: models.py:119
msgid "Workflow transition"
msgstr "Transição do Workflow"

#: models.py:120
msgid "Workflow transitions"
msgstr "Transição dos Workflows"

#: models.py:129
msgid "Document"
msgstr "Documento"

#: models.py:173 models.py:181
msgid "Workflow instance"
msgstr "Instância do Workflow"

#: models.py:174
msgid "Workflow instances"
msgstr "instâncias do Workflow "

#: models.py:184
msgid "Datetime"
msgstr "Hora e data"

#: models.py:196
msgid "Workflow instance log entry"
msgstr "Workflow instance log de entrada "

#: models.py:197
msgid "Workflow instance log entries"
msgstr "Workflow instance log de entradas"

#: permissions.py:7
msgid "Document workflows"
msgstr ""

#: permissions.py:10
msgid "Create workflows"
msgstr "Criar workflows"

#: permissions.py:13
msgid "Delete workflows"
msgstr "Excluir Workflows"

#: permissions.py:16
msgid "Edit workflows"
msgstr "Editar Workflows"

#: permissions.py:19
msgid "View workflows"
msgstr "Ver workflows"

#: permissions.py:26
msgid "Transition workflows"
msgstr ""

#: views.py:57
#, python-format
msgid "Workflows for document: %s"
msgstr "Workflows para documento: %s"

#: views.py:91
#, python-format
msgid "Documents with the workflow: %s"
msgstr ""

#: views.py:116
#, python-format
msgid "Detail of workflow: %(workflow)s"
msgstr "Detalhe do workflow: %(workflow)s"

#: views.py:162
msgid "Submit"
msgstr "Submeter"

#: views.py:164
#, python-format
msgid "Do transition for workflow: %s"
msgstr "Fazer a transição para o workflow: %s"

#: views.py:215
msgid "Available document types"
msgstr ""

#: views.py:216
msgid "Document types assigned this workflow"
msgstr ""

#: views.py:226
#, python-format
msgid "Document types assigned the workflow: %s"
msgstr "Os tipos de documento atribuído ao workflow: %s"

#: views.py:269
#, python-format
msgid "States of workflow: %s"
msgstr "Estado do workflow: %s"

#: views.py:287
#, python-format
msgid "Create states for workflow: %s"
msgstr "Criar estados para Workflow: %s"

#: views.py:363
#, python-format
msgid "Transitions of workflow: %s"
msgstr "Transição do Workflow: %s"

#: views.py:376
#, python-format
msgid "Create transitions for workflow: %s"
msgstr "Criar Transição para  Workflow: %s"

#: views.py:406
msgid "Unable to save transition; integrity error."
msgstr "Não foi possível salvar transição; erro de integridade."
