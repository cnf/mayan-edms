��    3      �  G   L      h  &   i  
   �     �     �     �     �     �     �  0        B  	   Q     [     w     |     �     �     �  
   �     �  "   �               !     1  
   9     D     _     k  ,   �  4   �  @   �     +     E     I     R  	   n     x     �     �     �     �     �     �  +        7     D     ]     a     d     p  �  �  <   .
     k
  @   w
     �
     �
  #   �
     	       C   1     u  
   �  -   �     �  *   �      �       
   2  
   =     H     c     �     �     �     �     �  *   �     �  %     -   :  8   h  N   �     �                    4     @  
   _     j     �     �     �     �  .   �                "     '     *     6     #   $                     .         %           *   &              0   '              )       2      
         +                   /              ,   	   "   1                -                                    (                 3   !                  Add new conditions to smart link: "%s" Conditions Conditions for smart link: %s Create condition Create new smart link Create new smart links Delete Delete smart links Document type for which to enable smart link: %s Document types Documents Documents in smart link: %s Edit Edit smart link condition Edit smart link: %s Edit smart links Enabled Expression Foreign document attribute Inverts the logic of the operator. Label Link condition Link conditions Negated Smart link Smart link query error: %s Smart links Smart links for document: %s The inclusion is ignored for the first item. This represents the metadata of all other documents. This smart link is not allowed for the selected document's type. View existing smart links and contains contains (case insensitive) ends with ends with (case insensitive) is equal to is equal to (case insensitive) is greater than is greater than or equal to is in is in regular expression is in regular expression (case insensitive) is less than is less than or equal to not or starts with starts with (case insensitive) Project-Id-Version: Mayan EDMS
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-04-27 14:13-0400
PO-Revision-Date: 2016-03-21 21:10+0000
Last-Translator: Roberto Rosario
Language-Team: Portuguese (Brazil) (http://www.transifex.com/rosarior/mayan-edms/language/pt_BR/)
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 Adiciona novas condições para a ligação inteligente: %s  condições Condições para a ligação inteligente: %s criado com sucesso. Criar Condição Criar novo link inteligente Criar novas ligações inteligentes Excluir Excluir ligações inteligentes Tipo de documento para o qual a permitir ligação inteligente:  %s Tipo de Documento Documentos Os documentos em referência inteligente: %s  Editar Editar condição de ligação Inteligente Editar Ligação inteligente: %s Editar ligações inteligentes habilitado expressão Atributo documento externo Inverte a lógica do operador. Label condição de ligação condições de ligação negada link inteligente Erro Links Inteligentes para documento: %s Ligações inteligentes Links Inteligentes para documento: %s A inclusão é ignorada para o primeiro item. Esta expressão será avaliada contra o documento atual. Este link inteligente não é permitido para o tipo de documento selecionado.  Ver os ligações inteligentes e contém contém (case insensitive) termina com termina com (case insensitive) é igual a é igual a (case insensitive) é maior do que é maior ou igual a está em está em expressão regular está em expressão regular (case insensitive) é inferior a é menor ou igual a não ou começa com começa com (case insensitive) 